package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.GlobalVariables;
import Beans.User;

/**
 * Servlet implementation class Connexion
 */
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Connexion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			User u = new User();
			u.setEmail(request.getParameter("email"));
			u.setPassword(request.getParameter("motdepasse"));
			for (int i = 0; i < GlobalVariables.getInstance().getArrayList().size(); i++) {
				if (GlobalVariables.getInstance().getArrayList().get(i).getEmail().equalsIgnoreCase(u.getEmail()) && GlobalVariables.getInstance().getArrayList().get(i).getPassword().equalsIgnoreCase(u.getPassword())) {
					HttpSession session = request.getSession(true);
					session.setAttribute("currentSessionUser", GlobalVariables.getInstance().getArrayList().get(i));
					response.sendRedirect("Affichage.jsp"); // logged-in page
				}
			}
			PrintWriter out = response.getWriter();
			out.print("<p style=' color : red; font-family: \"Century Gothic\"; position :absolute;top : 550px; left : 430px; z-index : 10000; '>Email ou mot de passe incorrect...</p>");
			request.getRequestDispatcher("index.jsp").include(request, response);
			out.close();
			/*request.setAttribute("erreur", "Email ou mot de passe incorrect...");
			RequestDispatcher dispatcher = request.getRequestDispatcher("Error.jsp");
			dispatcher.forward(request, response);*/

		} catch (Throwable theException) {
			System.out.println(theException);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
