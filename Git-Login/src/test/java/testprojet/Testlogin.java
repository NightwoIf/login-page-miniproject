package testprojet;

import junit.framework.TestCase;
import Beans.GlobalVariables;
import Beans.User;

public class Testlogin extends TestCase {
	private User utilisateur;

	protected void setUp() throws Exception {
		super.setUp();
		utilisateur = new User();
		utilisateur.setNom("Hamza");
		utilisateur.setEmail("dark@gmail.com");
		utilisateur.setPassword("123456");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		utilisateur = null;
	}

	public void testCheckIfExists() {
		for (int i = 0; i < GlobalVariables.getInstance().getArrayList().size(); i++) {
			if (GlobalVariables.getInstance().getArrayList().get(i).getEmail()
					.equalsIgnoreCase(utilisateur.getEmail())) {
				assertEquals("dark@gmail.com", GlobalVariables.getInstance().getArrayList().get(i).getEmail());
			}
		}
	}

	public void testRegistration() {
		assertEquals(true, GlobalVariables.getInstance().getArrayList().add(utilisateur));
	}

	public void testLogin() {
		for (int i = 0; i < GlobalVariables.getInstance().getArrayList().size(); i++) {
			if (GlobalVariables.getInstance().getArrayList().get(i).getEmail()
					.equalsIgnoreCase(utilisateur.getEmail())) {
				assertEquals("dark@gmail.com", GlobalVariables.getInstance().getArrayList().get(i).getEmail());
			}
		}
	}

}
