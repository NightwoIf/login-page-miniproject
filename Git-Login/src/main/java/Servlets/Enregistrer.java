package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.User;
import Beans.GlobalVariables;

/**
 * Servlet implementation class Enregistrer
 */
public class Enregistrer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Enregistrer() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			User u = new User();
			u.setEmail(request.getParameter("email"));
			u.setNom(request.getParameter("nom"));
			u.setPassword(request.getParameter("motdepasse"));
			for (int i = 0; i < GlobalVariables.getInstance().getArrayList().size(); i++) {
				if (GlobalVariables.getInstance().getArrayList().get(i).getEmail().equalsIgnoreCase(u.getEmail())) {
					request.setAttribute("erreur", "Sry , this user already exists !");
					RequestDispatcher dispatcher = request.getRequestDispatcher("Error.jsp");
					dispatcher.forward(request, response);
				}
			}
			GlobalVariables.getInstance().getArrayList().add(u);
			HttpSession session = request.getSession(true);
			session.setAttribute("currentSessionUser", u);
			response.sendRedirect("Affichage.jsp"); // logged-in page

		} catch (Throwable theException) {
			System.out.println(theException);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	public void addUser() {

	}

}
