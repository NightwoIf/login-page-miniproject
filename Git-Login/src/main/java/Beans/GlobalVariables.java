package Beans;

import java.util.ArrayList;

public class GlobalVariables {
	private ArrayList<User> listusers;

	private GlobalVariables() {
		listusers = new ArrayList<User>();
	}

	private static GlobalVariables instance;

	public static GlobalVariables getInstance() {
		if (instance == null)
			instance = new GlobalVariables();
		return instance;
	}
	public ArrayList<User> getArrayList(){
		return listusers;
	}

}
